# celonis/k8s-workshop
FROM node:carbon

# Install server
WORKDIR /app

COPY src ./src
COPY package.json ./
RUN yarn install

# Expose a port so we can access that from outside
EXPOSE 8080

# Run server
CMD [ "yarn", "start" ]
