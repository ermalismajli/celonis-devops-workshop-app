const http = require("http");
const express = require("express");
const httpServer = express();

const path = require('path');
const filePath = path.join(__dirname, 'config');
const fs = require("fs");
const serverImpl = http.createServer(httpServer);
serverImpl.listen("8080", () => {
    console.debug(`HTTP listening on 8080`);
});
httpServer.get("/", (request, response) => {
    const file = fs.readFileSync(filePath);
    response.send(file.toString());
});